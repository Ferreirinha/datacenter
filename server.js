const express = require("express");
const requireDir = require('require-dir');
const cors = require('cors');
const app = express();

const MongoClient = require("mongoose");
require('dotenv').config(); 
app.use(express.json());
app.use(cors());


app.listen(process.env.PORT || 3001, () => {
  console.log('rodando na porta 3001');
  MongoClient.connect(process.env.APP_BD, {
    useUnifiedTopology: true,
    useFindAndModify: true,
    useNewUrlParser: true,
    useCreateIndex: true
    
  }, (err) =>{
    if (err) throw err  
    console.log('conectado');
  } 
  )
});

requireDir('./src/models');

app.use('/apinode', require('./src/routes'));

